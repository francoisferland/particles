///
/// \file   ntree_impl.hpp 
/// \brief  NTree class implementation.
/// \author François Ferland (particles@francoisferland.com)
///
#ifndef NTREE_IMPL_HPP
#define NTREE_IMPL_HPP

#include <stack>
#include <boost/function.hpp>
#include <boost/bind.hpp>
#include <functional>
#include <cmath>

#ifdef PARTICLES_DIAGS_ON
#include <iostream>
#endif

namespace particles
{
    namespace impl
    {
        /// \brief Returns 2^n.
        unsigned long twoPow(const unsigned n)
        {
            return 1 << n;
        }

        /// \brief Return true if a < b in any dimension.
        template <const unsigned long N, typename S>
        bool less(
            const typename NTreeKey<N, S>::Type& a, 
            const typename NTreeKey<N, S>::Type& b)
        {
            for (unsigned int i = 0; i < N; ++i) {
                if (a[i] < b[i])
                    return true;
            }

            return false;
        }

        /// \brief Return true if two ranges intersects.
        template <const unsigned long N, typename S>
        bool intersects(
            const typename NTreeKey<N, S>::Type& a_min,
            const typename NTreeKey<N, S>::Type& a_max,
            const typename NTreeKey<N, S>::Type& b_min,
            const typename NTreeKey<N, S>::Type& b_max)
        {
            for (unsigned long i = 0; i < N; ++i) {
                if (a_max[i] < b_min[i])
                    return false;
                else if (b_max[i] <= a_min[i])
                    return false;
            }

            return true;
        }

        /// \brief Return the flattened (indexed) value of a key based on a mid
        /// point.
        ///
        /// Each dimension of the kit sets a flag.
        /// If the key value in a dimension is lower than the mid point, set to
        /// 0, otherwise 1.
        ///
        template <const unsigned long N, typename S>
        unsigned long flattenKey(
            const typename NTreeKey<N, S>::Type& k, 
            const typename NTreeKey<N, S>::Type& m)
        {
            unsigned long fk = 0;
            for (unsigned long i = 0; i < N; ++i) {
                fk = fk << 1;
                if (k[i] > m[i])
                    fk |= 1;
            }
            return fk;
        }

        /// \brief Retrieve the discrete location of a key based on the
        /// containing cell's starting corner and offset (mid-width).
        template <const unsigned long N>
        typename NTreeCellLocation<N>::Type keyLocation(
            const typename NTreeCellLocation<N>::Type& corner,
            const          unsigned long               offset,
            const          unsigned long               key)
        {
            typename NTreeCellLocation<N>::Type cl = corner;
            for (unsigned long j = 0; j < N; ++j) {
                if (key & (0x1 << (N - j - 1))) {
                    cl[j] += offset;
                }
            }

            return cl;
        }

        /// \brief Return the row-major index of a cell's location.
        template <const unsigned long N>
        unsigned long locationIndex(
            const typename NTreeCellLocation<N>::Type& loc,
            const unsigned long full_width)
        {
            unsigned long mp = 0; 
            for (
                unsigned long i = 0, stride = 1; 
                i < N; 
                ++i, stride *= full_width) {
                    mp += loc[i] * stride;
            }

            return mp;
        }

        /// \brief Return a new sub cell according to the given flattened key.
        ///
        /// \param cell Parent cell to base bounds on.
        /// \param fk   Flattened key.
        template<typename T, const unsigned long N, typename S>
        typename NTreeCell<T, N, S>::Ptr subdivide(
            NTreeCell<T, N, S>* cell,
            unsigned long       fk)
        {
            typedef NTreeCell<T, N, S> Cell;
            typename Cell::Key min, max;

            for (unsigned long i = 0; i < N; ++i) {
                unsigned long d = 0x1 << (N - i - 1);
                if (fk & d) {
                    min[i] = cell->mid[i];
                    max[i] = cell->max[i];
                } else {
                    min[i] = cell->min[i];
                    max[i] = cell->mid[i];
                }
            }

            return typename Cell::Ptr(new Cell(min, max));
        }

    }

    template <typename T, const unsigned long N, typename S>
    NTreeCell<T, N, S>::NTreeCell():
        child_cells(impl::twoPow(N), NULL)
    {
    }

    template <typename T, const unsigned long N, typename S>
    NTreeCell<T, N, S>::NTreeCell(
        const Key& mn, 
        const Key& mx):
            min(mn),
            max(mx),
            child_cells(impl::twoPow(N))
    {
        for (unsigned int i = 0; i < N; ++i)
            mid[i] = mn[i] / 2 + mx[i] / 2;
    }

    template <typename T, const unsigned long N, typename S, class A>
    NTree<T, N, S, A>::NTree(
        const Key& min, 
        const Key& max, 
        unsigned long max_depth):
            root_(min, max), 
            max_depth_(max_depth)
    {
#ifdef PARTICLES_DIAGS_ON
        std::cerr 
            << "NTree N: " << N 
            << ", max depth: "  << max_depth_
            << ", max leaves: " << maxLeavesCount()
            << std::endl;
#endif
    }

    template <typename T, const unsigned long N, typename S, class A>
    void NTree<T, N, S, A>::insert(const Key& k, const Value& v) 
        throw (NTreeOutOfBounds)
    {
        if (impl::less<N, S>(k, root_.min))
            throw NTreeOutOfBounds();
        if (impl::less<N, S>(root_.max, k))
            throw NTreeOutOfBounds();

        unsigned long depth = 0;
        Cell* cell = &root_;
        while (depth < max_depth_) {
            unsigned long fk = impl::flattenKey<N, S>(k, cell->mid);
            assert(fk < cell->child_cells.size());
            typename Cell::Ptr pc = cell->child_cells[fk];
            if (!pc) {
                pc = impl::subdivide(cell, fk);
                cell->child_cells[fk] = pc;
            }
            cell = pc.get();
            ++depth;
        }

        cell->values.push_back(std::make_pair(k, v));
    }

    template <typename T, const unsigned long N, typename S, class A>
    size_t NTree<T, N, S, A>::size() const
    {
        size_t s = 0;
        std::stack<const Cell*> stack;
        stack.push(&root_);
        while (!stack.empty()) {
            const Cell* cell = stack.top();
            stack.pop();
            s += cell->values.size();
            for (unsigned long i = 0; i < impl::twoPow(N); ++i) {
                Cell* cc = cell->child_cells[i].get();
                if (cc) {
                    stack.push(cc);
                }
            }
        }

        return s;
    }

    template <typename T, const unsigned long N, typename S, class A>
    void NTree<T, N, S, A>::find(
        const Key& k, 
        const S& radius, 
        boost::function<void (const std::pair<Key, Value>&)> fun) const
    {
        Key min, max;
        for (unsigned long i = 0; i < N; ++i) {
            min[i] = k[i] - radius;
            max[i] = k[i] + radius;
        }

#ifdef PARTICLES_DIAGS_ON
        size_t vc    = 0; // Cell visits count.
        size_t vcl   = 0; // Leaf cells visits count.
#endif

        std::stack<const Cell*> stack;
        stack.push(&root_);
        while (!stack.empty()) {
            const Cell* cell = stack.top();
            stack.pop();
            for (size_t i = 0; i < cell->values.size(); ++i) {
                bool in = true;
                const Key& vk = cell->values[i].first;
                for (unsigned long j = 0; (j < N && in); ++j) {
                    if (vk[j] < min[j]) 
                        in = false;
                    else if (vk[j] > max[j])
                        in = false;
                }

                if (in)
                    fun(cell->values[i]);
            }

#ifdef PARTICLES_DIAGS_ON
            if (!cell->values.empty())
                ++vcl;
#endif

            for (unsigned long i = 0; i < impl::twoPow(N); ++i) {
                Cell* c =  cell->child_cells[i].get();
                if (c && impl::intersects<N, S>(c->min, c->max, min, max))
                    stack.push(c);
            }

#ifdef PARTICLES_DIAGS_ON
            ++vc;
#endif
        }

#ifdef PARTICLES_DIAGS_ON
        std::cerr 
            << "vc: "    << vc  
            << ", vcl: " << vcl
            << std::endl;
#endif

    }

    template <typename T, const unsigned long N, typename S, class A>
    size_t NTree<T, N, S, A>::find(const Key& k, const S& radius) const
    {
        using namespace impl;

        struct Counter {
            static void fun(size_t* c, const std::pair<Key, Value>&) {
                ++(*c);
            }
        };

        size_t count = 0;
        find(k, radius, boost::bind(&Counter::fun, &count, _1));

        return count;
    }

    template <typename T, const unsigned long N, typename S, class A>
    void NTree<T, N, S, A>::clear(bool keep_tree)
    {
        if (keep_tree) {
            std::stack<Cell*> stack;
            stack.push(&root_);
            while (!stack.empty()) {
                Cell* cell = stack.top();
                stack.pop();
                cell->values.clear();
                for (unsigned long i = 0; i < impl::twoPow(N); ++i) {
                    Cell* c = cell->child_cells[i].get();
                    if (c) {
                        stack.push(c);
                    }
                }
            }
        } else {
            root_ = Cell(root_.min, root_.max);
        }

    }

    template <typename T, const unsigned long N, typename S, class A>
    template <class F>
    void NTree<T, N, S, A>::heatMap(std::vector<Scalar>& map) const
    {
        typedef          std::vector<Scalar>        HeatMap;
        typedef typename NTreeCellLocation<N>::Type Location;

        // TODO: Consider only clearing empty branches.
        map = HeatMap(maxLeavesCount(), 0);

        // Location-Offset pair:
        typedef std::pair<Location, unsigned long> Level;
        // Level-Cell pair:
        typedef std::pair<Level, const Cell*> StackPair;
        Location zero_loc;
        for (unsigned long i = 0; i < N; ++i) {
            zero_loc[i] = 0;
        }
        unsigned long full_width  = gridSize();
        unsigned long full_offset = full_width / 2;

        std::stack<StackPair> stack;
        stack.push(std::make_pair(
            std::make_pair(zero_loc, full_offset), 
            &root_));
        while (!stack.empty()) {
            StackPair cur = stack.top();
            stack.pop();
            const Location&     corner = cur.first.first;
            const unsigned long offset = cur.first.second;
            const Cell*         cell   = cur.second;

            if (!cell->values.empty()) {
                // Leaf cell, corner will be at the right location.
                unsigned long mp = impl::locationIndex<N>(corner, full_width);
                Scalar sum = 0;
                for (size_t i = 0; i < cell->values.size(); ++i) {
                    sum += F::fun(A::at(this, cell->values[i].second));
                }

                map[mp] = sum;

            } else if (!cell->child_cells.empty()) {
                for (unsigned long i = 0; i < impl::twoPow(N); ++i) {
                    const Cell* c = cell->child_cells[i].get();
                    if (!c)
                        continue;

                    Location new_loc = impl::keyLocation<N>(
                        corner, 
                        offset, 
                        i);
                    stack.push(std::make_pair(
                        std::make_pair(new_loc, offset / 2),
                        c));
                }
            }
        }
    }

    template <typename T, const unsigned long N, typename S, class A>
    unsigned long NTree<T, N, S, A>::maxLeavesCount() const
    {
        static unsigned long mlc = pow(impl::twoPow(N), max_depth_);
        return mlc;
    }

    template <typename T, const unsigned long N, typename S, class A>
    unsigned long NTree<T, N, S, A>::gridSize() const
    {
        static unsigned long gs = impl::twoPow(max_depth_);
        return gs;
    }


}

#endif

