#ifndef TEST_COMMON_HPP
#define TEST_COMMON_HPP

#include <iostream>
#include <time.h>

namespace test {
    template <class S>
    struct identity
    {
        static S fun(const S& v) { return v; }
    };

    void printTime(const clock_t& start, const char* text)
    {
        std::cerr << text << ": " <<
            1000.0 * (double(clock() - start) / CLOCKS_PER_SEC) << " ms" << 
            std::endl;
    }

}

#endif

