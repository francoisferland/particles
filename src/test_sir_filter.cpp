#include <particles/sir_filter.hpp>
#include "test_common.hpp"
#include <fstream>
#include <numeric>
#include <cmath>

namespace {
    void exportHeatMap(
        const std::vector<particles::SIRFilter2d::Scalar>& map,
        const unsigned long width,
        const char* fname)
    {
        std::ofstream file(fname);

        file << "data = [";

        unsigned long height = map.size() / width;
        for (size_t j = 0; j < (height-1); ++j) {
            for (size_t i = 0; i < (width-1); ++i) {
                file << map[width*j + i] << " ";
            }
            file << map[width*j + (width - 1)] << "," << std::endl;
        }
        for (size_t i = 0; i < (width-1); ++i) {
            file << map[width*(height-1) + i] << " ";
        }
        file << map[map.size()-1] << "];" << std::endl;
        file << "image(255*data);" << std::endl;
    }

    struct FixedGenerator
    {
        double x_, y_;

        FixedGenerator(double x, double y): x_(x), y_(y)
        {
        }

        particles::Particle<2> operator() () const
        {
            particles::Particle<2> p;
            p.position[0] = x_;
            p.position[1] = y_;

            return p;
        }

    };

}

int main(int argc, char** argv)
{
    using namespace particles;
    using namespace test;

    typedef Measurement<2>        MeasurementType;
    typedef MeasurementMixture<2> Mixture;

    MeasurementType y_null; // Default constructor test.

    SIRFilter2d::Vector pos_min, pos_max, vel_min, vel_max;

    pos_min[0] = pos_min[1] = -1.0;
    pos_max[0] = pos_max[1] =  1.0;

    vel_min = 0.5 * pos_min;
    vel_max = 0.5 * pos_max;
    
    UniformGenerator<2> gen(pos_min, pos_max, vel_min, vel_max);
    SIRFilter2d filter(pos_min, pos_max, gen, 6);

    pos_min << -0.05, -0.05;
    pos_max <<  0.05,  0.05;
    vel_min /= 10.0;
    vel_max /= 10.0;
    UniformGenerator<2> ng(pos_min, pos_max, vel_min, vel_max);

    clock_t start; 
    
    start = clock();
    filter.particlesCount(5e3);
    filter.lThreshold(1e-6);
    filter.nThreshold(2e3);
    filter.resampleOnOOB(true);
    filter.shouldResample(true);
    filter.init();
    printTime(start, "Init time");

    MeasurementType::Vector     y0_mean, y1_mean;
    MeasurementType::Covariance y_cov;
    y0_mean << -0.50, 0.00;
    y1_mean <<  0.50, 0.00;
    y_cov   <<  0.02, 0.01, 0.01, 0.02;

    MeasurementType y0(y0_mean, y_cov);
    MeasurementType y1(y1_mean, y_cov);
    Mixture         y_mix;
    y_mix.add(y0);
    y_mix.add(y1);

    for (int i = 0; i< 3; ++i) {
        start = clock();
        filter.update(0.1, y_mix, ng);
        printTime(start, "Mixture update time");
    }

    // Disabled transform test:
    // SIRFilter2d::TransformType tf;
    // tf.translation << 0.0, 0.5;
    // tf.rotation    << 0.0, -1.0, 1.0, 0.0;
    // filter.transform(tf);

    std::vector<SIRFilter2d::Scalar> heat_map;
    start = clock();
    filter.heatMap(heat_map);
    printTime(start, "Heat map generation time");
    double hm_sum = 
        std::accumulate(heat_map.begin(), heat_map.end(), 0.0);
    if (fabs(hm_sum - 1.0) > 1e-6) {
        std::cerr << "Error: Total weight sum in heat map is not 1.0, " 
            "got " << hm_sum << "." << std::endl;
    }

    const SIRFilter2d::ParticleType& heaviest = filter.top();
    std::cerr << "Heaviest particle pos: (" << heaviest.position[0] << ", "
                                            << heaviest.position[1] << "), w: "
                                            << heaviest.weight << "."
                                            << std::endl;

    const char* fname = (argc > 1) ? argv[1] : "./heatmap.m";
    std::cerr << "Exporting heat map as Matlab matrix in '" << fname << "'"
        << std::endl;
    exportHeatMap(heat_map, filter.gridSize(), fname);

    return 0;
}
