///
/// \file   sir_filter_impl.hpp 
/// \brief  SIRFilter class implementation.
/// \author François Ferland (particles@francoisferland.com)
///

#ifndef SIR_FILTER_IMPL_HPP
#define SIR_FILTER_IMPL_HPP

#include <Eigen/LU>
#include <cstdlib>
#include <limits>
#include <boost/math/special_functions/fpclassify.hpp>

namespace particles
{
    namespace impl
    {
        /// \brief A functor that returns the weight of a given particle.
        ///
        /// Straight access, used as a function pointer, in heatMap for
        /// instance.
        template <const unsigned long N>
        struct ParticleWeightFun
        {
            typedef          Particle<N>     ParticleType;
            typedef typename ParticleType::S Scalar;

            Scalar operator() (const ParticleType& p)
            {
                return fun(p);
            }

            static Scalar fun(const ParticleType& p)
            {
                return p.weight;
            }
        };

        /// \brief Saturates a particle's position between the given bounds.
        ///
        /// \return True if saturation occured.
        template <const unsigned long N>
        bool saturatePosition(
            Particle<N>& p,
            const typename Particle<N>::Vector& min,
            const typename Particle<N>::Vector& max)
        {
            bool changed = false;
            for (unsigned long j = 0; j < N; ++j) {
                if (p.position[j] < min[j]) {
                    p.position[j] = min[j];
                    changed = true;
                } else if (p.position[j] > max[j]) {
                    p.position[j] = max[j];
                    changed = true;
                }
            }

            return changed;
        }
    }

    template <const unsigned long N>
    void Particle<N>::update(const S dt)
    {
        position += velocity * dt;
    }

    template <const unsigned long N>
    void ParticleTransform<N>::apply(Particle<N>& p) const
    {
        p.position  = rotation * p.position;
        p.velocity  = rotation * p.velocity;
        p.position += translation; 
    }

    template <const unsigned long N>
    Measurement<N>::Measurement(const Vector& mean, const Covariance& cov):
        mean_(mean), 
        cov_(cov), 
        cov_i_(cov.inverse())
    {
        init();
    }

    template <const unsigned long N>
    Measurement<N>::Measurement():
        mean_(0,0),
        cov_(Covariance::Identity()),
        cov_i_(cov_)
    {
        init();
    }

    template <const unsigned long N>
    void Measurement<N>::init()
    {
        static const Scalar piN = pow(M_PI, N);

        factor_ = 1.0 / sqrt(2.0 * piN * cov_.determinant());
    }

    template <const unsigned long N>
    typename Measurement<N>::Scalar Measurement<N>::eval(
        const typename Measurement<N>::Vector& x) const
    {
        Vector dist = x - mean_;
        return factor_ * exp(-0.5 * (dist.transpose() * cov_i_ * dist)(0,0));
    }

    template <const unsigned long N>
    void MeasurementMixture<N>::add(const MeasurementType& m)
    {
        elems_.push_back(m);
    }

    template <const unsigned long N>
    void MeasurementMixture<N>::clear()
    {
        elems_.clear();
    }

    template <const unsigned long N>
    bool MeasurementMixture<N>::empty() const
    {
        return elems_.empty();
    }

    template <const unsigned long N>
    typename MeasurementMixture<N>::Scalar MeasurementMixture<N>::eval(
        const typename MeasurementMixture<N>::Vector& x) const
    {
        double s = 0.0;
        typedef typename std::vector<MeasurementType>::const_iterator It;
        for (It i = elems_.begin(); i != elems_.end(); ++i) {
            s += i->eval(x);
        }

        return s /= elems_.size();
    }

    template <const unsigned long Nd, class P>
    typename SIRFilterModel<Nd, P>::Key SIRFilterModel<Nd, P>::key(const T& v)
    {
        return key(v.position);
    }

    template <const unsigned long Nd, class P>
    typename SIRFilterModel<Nd, P>::Key SIRFilterModel<Nd, P>::key(
        const typename T::Vector& v)
    {
        typename SIRFilterModel<Nd, P>::Key k;
        std::copy(&v.data()[0], &v.data()[Nd], &k[0]);
        return k;
    }

    template <const unsigned long N>
    UniformGenerator<N>::UniformGenerator(
        const Vector& pos_min, 
        const Vector& pos_max,
        const Vector& vel_min, 
        const Vector& vel_max):
            pos_min_(pos_min),
            pos_range_(pos_max - pos_min),
            vel_min_(vel_min),
            vel_range_(vel_max - vel_min)
    {
#ifdef srandomdev // Available on BSD-derived platforms.
        srandomdev();
#endif
    }

    template <const unsigned long N>
    typename UniformGenerator<N>::Scalar UniformGenerator<N>::uniRand() const
    {
        return Scalar(random()) / Scalar(RAND_MAX);
    }

    template <const unsigned long N>
    Particle<N> UniformGenerator<N>::operator()() const
    {
        Particle<N> p;
        for (unsigned long i = 0; i < N; ++i) {
            p.position[i] = pos_min_[i] + uniRand() * pos_range_[i];
            p.velocity[i] = vel_min_[i] + uniRand() * vel_range_[i];
        }
                    
        return p;
    }

    template <const unsigned long N, class P, class G>
    SIRFilter<N, P, G>::SIRFilter(
        const Vector&       min, 
        const Vector&       max,
        const Generator&    gen,
        const unsigned long max_depth):
            generator_(gen),
            data_(Model::key(min), Model::key(max), max_depth),
            effective_n_(0.0),
            particles_count_(1000),
            min_(min),
            max_(max),
            l_threshold_(1e-6),
            n_threshold_(particles_count_ / 2.0),
            resample_on_oob_(true)
    {
        data_.reserve(particles_count_);
    }

    template <const unsigned long N, class P, class G>
    void SIRFilter<N, P, G>::particlesCount(const unsigned long n)
    {
        particles_count_ = n;
    }

    template <const unsigned long N, class P, class G>
    void SIRFilter<N, P, G>::lThreshold(const Scalar lt)
    {
        l_threshold_ = lt;
    }

    template <const unsigned long N, class P, class G>
    void SIRFilter<N, P, G>::nThreshold(const Scalar nt)
    {
        n_threshold_ = nt;
    }

    template <const unsigned long N, class P, class G>
    typename SIRFilter<N, P, G>::Scalar SIRFilter<N, P, G>::effectiveN() const
    {
        return effective_n_;
    }

    template <const unsigned long N, class P, class G>
    void SIRFilter<N, P, G>::shouldResample(const bool v)
    {
        resample_ = v;
    }

    template <const unsigned long N, class P, class G>
    void SIRFilter<N, P, G>::resampleOnOOB(const bool v)
    {
        resample_on_oob_ = v;
    }

    template <const unsigned long N, class P, class G>
    template <class Gc>
    void SIRFilter<N, P, G>::init(Gc& gen)
    {
        Scalar w = 1.0 / particles_count_;
        for (size_t i = 0; i < particles_count_; ++i) {
            ParticleType p = gen();
            p.weight = w;
            data_.insert(p);
        }
    }

    template <const unsigned long N, class P, class G>
    void SIRFilter<N, P, G>::init()
    {
        init(generator_);
    }

    template <const unsigned long N, class P, class G>
    void SIRFilter<N, P, G>::transform(const TransformType& t)
    {
        typedef typename ContainerType::iterator It;
        for (It i = data_.begin(); i != data_.end(); ++i) {
            t.apply(*i);
            if (impl::saturatePosition<N>(*i, min_, max_)) {
                i->weight = 0.0;
            }
        }
    }

    template <const unsigned long N, class P, class G>
    template <class M, class Gn>
    void SIRFilter<N, P, G>::update(const Scalar dt, const M& m, Gn& gen)
    {
#ifdef PARTICLES_DIAGS_ON
        size_t oob_resamplings = 0;
#endif
        typedef typename ContainerType::iterator It;
        for (It i = data_.begin(); i != data_.end(); ++i) {
            i->update(dt);
            if (impl::saturatePosition<N>(*i, min_, max_)) {
                if (resample_on_oob_) {
                    (*i) = generator_();
                    i->weight = 1.0 / particles_count_;
#ifdef PARTICLES_DIAGS_ON
                    ++oob_resamplings;
#endif
                } else {
                    i->weight = 0.0;
                }
            }
        }

#ifdef PARTICLES_DIAGS_ON
        if (oob_resamplings) {
            std::cerr << "OOB resamplings: " << oob_resamplings << std::endl;
        }
#endif

        // Apply effect of measurement on weight.
        
        Scalar sum_w = 0.0;

        for (It i = data_.begin(); i != data_.end(); ++i) {
            Vector& x    = i->position;
            Scalar& w    = i->weight;

            Scalar pdf = m.eval(x);
            if (pdf > l_threshold_)
                w *= pdf;

            sum_w   += w;
        }

        // Normalize weights to a sum of 1.
        // Also look for the heaviest particle.
        Scalar max_weight = 0;
        // Sum of squares of normalized weights, used in effective sample size
        // test:
        Scalar sum_w_2 = 0.0; 

        It     heaviest   = data_.begin();
        for (It i = data_.begin(); i != data_.end(); ++i) {
            Scalar& w = i->weight;

            w       /= sum_w;
            sum_w_2 += w * w;

            if (w > max_weight) {
                heaviest   = i;
                max_weight = w; 
            }
        }

        // Keep a copy of the heaviest particle, since weights are all equal
        // after resampling.
        heaviest_ = *heaviest;

        // Test if an invalid value (or simply zero) value was introduced before
        // propagating NaNs.
        effective_n_ = boost::math::isnormal(sum_w_2) ? 1.0 / sum_w_2
                                                      : 0.0;

#ifdef PARTICLES_DIAGS_ON
        std::cerr << "heaviest particle weight: " << heaviest_.weight << 
            std::endl;
        std::cerr << "Effective sample size:    " << effective_n_ <<
            std::endl; 
#endif

        if (resample_ && (effective_n_ < n_threshold_)) {
            resample(gen);
        }

    }

    template <const unsigned long N, class P, class G>
    template <class Gn>
    void SIRFilter<N, P, G>::resample(Gn& gen)
    {
        Scalar w_n = 1.0 / data_.size(); // Normalized weight after resampling.

        std::vector<Scalar>       c(data_.size());
        std::vector<ParticleType> new_data(data_.size());

        c[0] = 0.0;
        for (size_t i = 1; i < data_.size(); ++i) {
            c[i] = c[i-1] + data_[i].weight;
        }

        size_t i = 0;
        Scalar u_j = 0;
        for (size_t j = 0; j < data_.size(); ++j) {
            u_j += w_n;
            while (u_j > c[i] && (i < (data_.size() - 1))) {
                ++i;
            }

            ParticleType noise = gen();
            new_data[j] = data_[i];
            new_data[j].weight = w_n;
            new_data[j].position += noise.position;
            new_data[j].velocity += noise.velocity;
            impl::saturatePosition<N>(new_data[j], min_, max_);
        }

        std::copy(new_data.begin(), new_data.end(), data_.begin());
    }

    template <const unsigned long N, class P, class G>
    const typename SIRFilter<N, P, G>::ParticleType& 
        SIRFilter<N, P, G>::top() const
    {
        return heaviest_;
    }

    template <const unsigned long N, class P, class G>
    void SIRFilter<N, P, G>::heatMap(std::vector<Scalar>& map)
    {
        data_.reindex();
        data_.template heatMap< impl::ParticleWeightFun<N> >(map);
    }

    template <const unsigned long N, class P, class G>
    unsigned long SIRFilter<N, P, G>::gridSize() const
    {
        return data_.gridSize();
    }

}

#endif

