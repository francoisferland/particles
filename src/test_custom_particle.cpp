#include <particles/sir_filter.hpp>
#include "test_common.hpp"
#include <fstream>
#include <numeric>
#include <cmath>

namespace {
    struct CustomParticle: public particles::Particle<2>
    {
        double custom_field;
    };

    class CustomGenerator
    {
    public:
        typedef CustomParticle::S      Scalar;
        typedef CustomParticle::Vector Vector;

    private:
        Vector pos_min_;
        Vector pos_range_;
        Vector vel_min_;
        Vector vel_range_;

        particles::UniformGenerator<2> unigen_;

    public:
        CustomGenerator(
            const Vector& pos_min, 
            const Vector& pos_max,
            const Vector& vel_min, 
            const Vector& vel_max):
                unigen_(pos_min, pos_max, vel_min, vel_max)
        {
        }

        CustomParticle operator()() const
        {
            particles::Particle<2> p_org = unigen_();
            CustomParticle p;
            p.position       = p_org.position;
            p.velocity       = p_org.velocity;
            p.weight         = p_org.weight;
            p.custom_field   = 5.0;

            return p;
        }

    };

}

int main(int argc, char** argv)
{
    using namespace particles;
    using namespace test;

    typedef Measurement<2>        MeasurementType;
    typedef MeasurementMixture<2> Mixture;

    typedef SIRFilter<2, CustomParticle, CustomGenerator> SIRFilterType;

    SIRFilterType::Vector pos_min, pos_max, vel_min, vel_max;

    pos_min[0] = pos_min[1] = -1.0;
    pos_max[0] = pos_max[1] =  1.0;

    vel_min = 0.5 * pos_min;
    vel_max = 0.5 * pos_max;
    
    CustomGenerator gen(pos_min, pos_max, vel_min, vel_max);
    SIRFilterType   filter(pos_min, pos_max, gen, 6);

    pos_min << -0.05, -0.05;
    pos_max <<  0.05,  0.05;
    vel_min /= 10.0;
    vel_max /= 10.0;
    CustomGenerator ng(pos_min, pos_max, vel_min, vel_max);

    clock_t start; 
    
    start = clock();
    filter.particlesCount(5e3);
    filter.lThreshold(1e-6);
    filter.shouldResample(true);
    filter.init(gen);
    printTime(start, "Init time");

    MeasurementType::Vector     y0_mean, y1_mean;
    MeasurementType::Covariance y_cov;
    y0_mean << -0.50, 0.00;
    y1_mean <<  0.50, 0.00;
    y_cov   <<  0.02, 0.01, 0.01, 0.02;

    MeasurementType y0(y0_mean, y_cov);
    MeasurementType y1(y1_mean, y_cov);
    Mixture         y_mix;
    y_mix.add(y0);
    y_mix.add(y1);

    for (int i = 0; i< 3; ++i) {
        start = clock();
        filter.update(0.1, y_mix, ng);
        printTime(start, "Mixture update time");
    }

    // Disabled transform test:
    // SIRFilter2d::TransformType tf;
    // tf.translation << 0.0, 0.5;
    // tf.rotation    << 0.0, -1.0, 1.0, 0.0;
    // filter.transform(tf);

    std::vector<SIRFilter2d::Scalar> heat_map;
    start = clock();
    filter.heatMap(heat_map);
    printTime(start, "Heat map generation time");
    double hm_sum = 
        std::accumulate(heat_map.begin(), heat_map.end(), 0.0);
    if (fabs(hm_sum - 1.0) > 1e-6) {
        std::cerr << "Error: Total weight sum in heat map is not 1.0, " 
            "got " << hm_sum << "." << std::endl;
    }

    const SIRFilterType::ParticleType& heaviest = filter.top();
    std::cerr << "Heaviest particle pos: (" << heaviest.position[0] << ", "
                                            << heaviest.position[1] << "), w: "
                                            << heaviest.weight << ", cf: "
                                            << heaviest.custom_field << "."
                                            << std::endl;

    return 0;
}
