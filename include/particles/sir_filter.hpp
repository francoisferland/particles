///
/// \file   sir_filter.hpp 
/// \brief  SIRFilter-related classes.
/// \author François Ferland (particles@francoisferland.com)
///
/// Related tests won't get built if Eigen is not available.
/// 

#ifndef SIR_FILTER_HPP
#define SIR_FILTER_HPP

#include "common.hpp"
#include "container.hpp"
#include <Eigen/Core>

namespace particles
{
    
    // Forward declaration:
    template <const unsigned long N> struct ParticleTransform;

    /// \brief A particle with position and velocity.
    template <const unsigned long N>
    struct Particle
    {
        typedef double S;
        typedef Eigen::Matrix<S, N, 1> Vector;

        S      weight;
        Vector position;
        Vector velocity;

        /// \brief Update phase of SIR algorithm.
        ///
        /// Applies position += dt * velocity.
        void update(const S dt);

    };

    /// \brief Storage for a particle's position and velocity transform.
    ///
    /// Rotation is meant to be applied before translation.
    ///
    template <const unsigned long N>
    struct ParticleTransform
    {
        typedef typename Particle<N>::S S;
        typedef Eigen::Matrix<S, N, 1>  Vector;
        typedef Eigen::Matrix<S, N, N>  Matrix;

        Matrix rotation;
        Vector translation;

        void apply(Particle<N>& p) const;
    };

    /// \brief A single normal distribution measurement.
    template <const unsigned long N>
    class Measurement
    {
    public:
        typedef typename Particle<N>::S               Scalar;
        typedef typename Particle<N>::Vector          Vector; 
        typedef          Eigen::Matrix<Scalar, N, N>  Covariance;

    private:
        Vector     mean_;
        Covariance cov_;
        Covariance cov_i_;
        Scalar     factor_;

    public:
        /// \brief Constructor with parameters.
        Measurement(const Vector& mean, const Covariance& cov);

        /// \brief Default constructor.
        ///
        /// Produces a measurement with a zero mean and an identity matrix as
        /// covariance. 
        Measurement();

        /// \brief Evaluate the probablity density function for x.
        Scalar eval(const Vector& x) const;

    private:
        void init();

    };

    /// \brief A mixture of normal distribution measurements.
    template <const unsigned long N>
    class MeasurementMixture
    {
    public:
        typedef          Measurement<N>          MeasurementType;
        typedef typename MeasurementType::Scalar Scalar;
        typedef typename MeasurementType::Vector Vector;

    private:
        std::vector<MeasurementType> elems_;

    public:
        /// \brief Add a single measurement to the mixture set.
        void add(const MeasurementType& m);

        /// \brief Clear the mixture set.
        void clear();

        /// \brief Return true if the current mixture is empty.
        bool empty() const;

        /// \brief Evaluate a vector with this mixture, return the probability
        /// density value.
        Scalar eval(const Vector& p) const;

    };

    /// \brief An uniform distribution particle generator.
    ///
    /// Resets seed with randdev() in constructor.
    ///
    template <unsigned long N>
    class UniformGenerator
    {
    public:
        typedef typename Particle<N>::S      Scalar;
        typedef typename Particle<N>::Vector Vector;

    private:
        Vector pos_min_;
        Vector pos_range_;
        Vector vel_min_;
        Vector vel_range_;

    public:
        UniformGenerator(
            const Vector& pos_min, 
            const Vector& pos_max,
            const Vector& vel_min, 
            const Vector& vel_max);

        Particle<N> operator()() const;

        Scalar uniRand() const; 

    };

    /// \brief Storage model for SIRFilter.
    /// 
    /// Used as a link between SIRFilter and Container storage class.
    ///
    /// Template parameters:
    /// 
    ///  - Nd: Number of dimensions.
    ///  - P:  Particle type, should match the P parameter of SIRFilter<N, P>.
    ///
    /// Note: The multiple typedefs are used by the Container class.
    ///
    template <const unsigned long Nd, class P>
    class SIRFilterModel
    {
    public:
        static const unsigned long N = Nd;

        typedef          P                    T;
        typedef typename T::S                 S;
        typedef typename NTreeKey<N, S>::Type Key;

        static Key key(const T&);
        static Key key(const typename T::Vector& v);
    };

    /// \brief A N-dimension(s) SIR particle filter.
    ///
    /// Template parameters: 
    ///  - N: Number of dimensions.
    ///  - P: Particle type, see Particle<N> for requirements.
    ///
    /// NOTE: The current implementation always resamples the particles set.
    ///
    /// The number of particles can be adjusted with particlesCount(...).
    /// The default is 1000.
    ///
    template <
        unsigned long N, 
        class         P = Particle<N>, 
        class         G = UniformGenerator<N> >
    class SIRFilter
    {
    public:
        typedef          SIRFilterModel<N, P> Model;
        typedef          Container<Model>     ContainerType;
        typedef          P                    ParticleType;
        typedef          ParticleTransform<N> TransformType;
        typedef typename ParticleType::S      Scalar;
        typedef typename ParticleType::Vector Vector;
        typedef          G                    Generator;

    private:
        Generator     generator_;
        ContainerType data_;
        ParticleType  heaviest_;
        Scalar        effective_n_;

        unsigned long particles_count_;
        Vector        min_;
        Vector        max_;
        Scalar        l_threshold_;
        Scalar        n_threshold_;
        bool          resample_;
        bool          resample_on_oob_;

        /// \brief Resample the particles set.
        ///
        /// The Gn template parameter is a noise generator for "in-place" 
        /// resampling.
        template <class Gn>
        void resample(Gn& gen);

    public:
        /// \brief Constructor.
        ///
        /// Bounds are only used for particle positions in the container.
        /// Velocity bounds are fixed by the particles' generator and updater.
        ///
        /// \param max_depth Maximum subdivision depth for the underlying tree
        /// structure.
        SIRFilter(
            const Vector&       min, 
            const Vector&       max,
            const G&            gen,
            const unsigned long max_depth = 4);

        /// \brief Total particles count.
        ///
        /// Will always operate on that amount.
        /// Used when initializing and resampling the population.
        void particlesCount(const unsigned long n);

        /// \brief Likelihood threshold when applying measurement probabilities
        /// p(y | x).
        ///
        /// Only apply w_k = w_{k-1} * p(y_k | x_{k}) if p(y_k | x_{k}) is over
        /// this threshold.
        /// Useful if measurements do not cover the whole state.
        /// Its default value is 1e-6.
        /// 
        void lThreshold(const Scalar lt);

        /// \brief Effective sample size threshold for resampling.
        ///
        /// If resampling is enabled (see shouldResample, true by default), the
        /// particle population is resampled according to the current
        /// distribution and a noise generator given to update().
        /// The effective sample size is obtained with 1.0 / sum_i_N(w_i)^2, 
        /// where w_i is the normalized weight of particle i.
        ///
        /// To systematically resample every cycle, set this value over the
        /// total particles count.
        /// The default value is half the default total particles count (500.0).
        ///
        void nThreshold(const Scalar nt);

        /// \brief Return the result of the last effective sample size
        /// estimation.
        ///
        /// See nThreshold() for details.
        ///
        Scalar effectiveN() const;

        /// \brief Indicates if we should resample or not. 
        ///
        /// Can be used to turn off resampling.
        /// The default is true since it would not be a SIR filter without
        /// resampling.
        void shouldResample(bool v);

        /// \brief Indicates if a particle should be resampled when it goes 
        /// out-of-bounds.
        ///
        /// If true, a new particle is created with the default generator.
        /// Otherwise, the particle's weight is set to 0.0.
        /// The default is true.
        ///
        void resampleOnOOB(bool v);

        /// \brief Intialize the population with the given generator.
        ///
        /// The generator has to act as a function object with no parameters and
        /// return a new ParticleType.
        /// See UniformGenerator<> for an example. 
        ///
        template <class Gc>
        void init(Gc& gen);

        /// \brief Initialize the population with the default generator given in
        /// the constructor.
        void init();

        /// \brief Alter all particles with the given transform, discard those
        /// that are moved out of bounds.
        void transform(const TransformType& t);

        /// \brief Update with the given time interval and single measurement.
        ///
        /// Calls M::eval(x) to obtain the value of p(x|y). 
        /// Calls Particle::update(dt) on each particle and re-index the tree.
        ///
        /// Performs in-place resampling according to the current distribution
        /// and adds noise by calling gen().
        ///
        /// \param dt  Time difference between updates.
        /// \param y   Measurement to apply.
        /// \param gen Resampling generator.
        ///            The generation result is added to selected particles.
        template <class M, class Gn>
        void update(const Scalar dt, const M& y, Gn& gen);

        /// \brief Return a reference to the single particle with the highest
        /// weight.
        ///
        /// Note: the given particle might not be part of the set, as a copy of
        /// the heaviest particle is kept before resampling.
        ///
        const ParticleType& top() const;

        /// \brief Produces an heat map based on the weight of the particles.
        ///
        /// Data is stored in row-major order and will be normalized in each
        /// dimension.
        /// See NTree implementation for details.
        /// 
        /// NOTE: Forces rebuild of the tree prior to heatmap generation, since
        /// it's structure might not reflect changes made to the particles.
        ///
        void heatMap(std::vector<Scalar>& map);

        /// \brief Return the normalized, discretized grid size in each 
        /// dimension.
        ///
        /// Based on the underlying tree structure, useful for methods such as 
        /// heatMap.
        unsigned long gridSize() const;

    };

    typedef SIRFilter<2> SIRFilter2d;
    typedef SIRFilter<3> SIRFilter3d;

}

#include "sir_filter_impl.hpp"

#endif

