///
/// \file   container.hpp 
/// \brief  NTree indexer with a flat vector container.
/// \author François Ferland (particles@francoisferland.com)
///

#ifndef CONTAINER_HPP
#define CONTAINER_HPP

#include "ntree.hpp"

namespace particles
{
    /// \brief A N-dimension container that uses a NTree as a retrieval
    /// mechanism.
    /// 
    /// Only one template parameter, M, to describe the model used:
    ///
    /// const unsigned long        M::N:             Dimensions count.
    /// typedef                    M::T:             Value type to store.
    /// typedef                    M::S:             Scalar type used to index.
    /// static NTreeKey<N, Scalar> M::key(const T&): Create a key from a value.
    ///
    template <class M>
    class Container: public NTree< size_t, M::N, typename M::S, Container<M> >
    {
    public:
        static const unsigned long N = M::N;

        typedef          Container<M>              ThisType;
        typedef typename M::T                      Value;
        typedef typename M::S                      Scalar;
        typedef typename NTreeKey<N, Scalar>::Type Key;
        typedef          NTree<size_t, N, Scalar, ThisType>  Tree;

        typedef          std::vector<Value>        Vector;
        typedef typename Vector::iterator          iterator;
        typedef const    iterator                  const_iterator;

    private:
        //Tree               tree_; 
        std::vector<Value> data_;

    public:
        Container(
            const Key& min, 
            const Key& max, 
            const unsigned long max_depth = 8);

        /// \brief Insert an element into the container.
        /// 
        /// Copies the given value in a vector and store the index into the
        /// retrieval tree.
        void insert(const Value& v);

        /// \brief Reindex the whole tree based on keys obtained from the data
        /// vector.
        ///
        /// Useful if information about the contained data changes its position 
        /// in the tree.
        void reindex();

        // Vector interface:
        iterator       begin();
        const_iterator begin() const;
        iterator       end();
        const_iterator end() const;
        const Value&   at(const size_t i) const;
        const Value&   operator[](const size_t i) const;
        void           reserve(const size_t s);
        size_t         size() const;

        /// \brief Produces an heat map of the contained values with the given
        /// function.
        ///
        /// See declaration in NTree for details.
        void heatMap(
            std::vector<Scalar>& map,
            boost::function<Scalar (const Value& v)> fun) const;

        /// \brief A template version of heatMap.
        ///
        /// See declaration in NTree for details.
        template <class F>
        void heatMap(std::vector<Scalar>& map) const;
        
        /// \brief Return the discrete grid size used in the underlying tree
        /// structure.
        ///
        /// Corresponds to 2^max_depth.
        unsigned long gridSize() const;

        template <class C>
        static const Value& at(const C& c, size_t i)
        {
            return static_cast<const ThisType*>(c)->at(i);
        }

    };

}

// Separate implementation:
#include "container_impl.hpp"

#endif

