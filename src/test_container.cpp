#include <particles/container.hpp>

namespace {
    struct Value
    {
        double x,y;
        double v;
    };

    class Model
    {
    public:
        typedef Value  T;
        typedef double S;
        static const unsigned long N = 2;

        typedef particles::NTreeKey<N, S>::Type Key;

        static Key key(const Value& v)
        {
            Key k = {v.x, v.y};
            return k;
        }

    };
}

int main(int argc, char** argv)
{
    using namespace particles;

    Model::Key min = {-1.0, -1.0};
    Model::Key max = { 1.0,  1.0};
    Container<Model> c(min, max);

    Value v1 = {-0.5, 0.0, 1.0};
    c.insert(v1);

}

