///
/// \file   ntree.hpp 
/// \brief  NTree indexer structure.
/// \author François Ferland (particles@francoisferland.com)
///

#ifndef NTREE_HPP
#define NTREE_HPP

#include <boost/array.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/function.hpp>
#include <vector>
#include <map>  // For std::pair.
#include <limits>
#include <stdexcept>

namespace particles
{
    /// \brief Standard runtime error type.
    typedef std::runtime_error NTreeException;
    /// \brief Out of bounds exception when accessing a NTree structure.
    struct NTreeOutOfBounds: public NTreeException 
    {
        NTreeOutOfBounds(): NTreeException("Out of bounds.") {}
    };

    /// \brief Construct a valid key type for NTree and related classes.
    ///
    /// The result is in the Type typedef.
    template <const unsigned long N, typename S>
    struct NTreeKey
    {
        typedef boost::array<S, N> Type;
    };

    /// \brief A cell's disrete position type.
    ///
    /// The result is in the Type typedef.
    template <unsigned long N>
    struct NTreeCellLocation
    {
        typedef boost::array<unsigned long, N> Type;
    };

    /// \brief A single value holder cell for NTree.
    template <typename T, const unsigned long N, typename S>
    struct NTreeCell
    {
        typedef typename NTreeKey<N, S>::Type           Key;
        typedef boost::shared_ptr< NTreeCell<T, N, S> > Ptr;
        typedef const Ptr                               ConstPtr;
        typedef std::vector<Ptr>                        ChildCells;
        typedef std::pair<Key, T>                       Value;
        typedef std::vector<Value>                      Values;

        Key        min;
        Key        max;
        Key        mid;
        ChildCells child_cells;
        Values     values;

        /// \brief Default constructor.
        ///
        /// Bounds are undefined, child cells are initialized with null
        /// pointers.
        NTreeCell();

        /// \brief Full constructor.
        ///
        /// Bounds are defined, and child cells are initialized with null
        /// pointers.
        NTreeCell(const Key& mn, const Key& mx);
    };

    template <typename T>
    struct IdentityStorageAdapter
    {
        template <class C>
        static       T& at(      C* c,       T& t) { return t; }
        template <class C>
        static const T& at(const C* c, const T& t) { return t; }
    };

    /// \brief A N-dimensions tree structure with bounded volume.
    /// 
    /// Corresponds to a quadtree in 2D, an octotree in 3D, etc.
    /// Data-agnostic.
    ///
    /// Template parameters:
    ///
    ///  - T: The data type.
    ///  - N: The dimension count.
    ///       Defines the key type.
    ///  - S: Scalar type used for coordinates, defaults to double.
    ///  - A: Storage adapter, needs to implement A::at(T).  
    template<
        typename            T, 
        const unsigned long N, 
        typename            S = double, 
        class               A = IdentityStorageAdapter<T> >
    class NTree
    {
    public:
        typedef S                              Scalar;
        typedef T                              Value;
        typedef typename NTreeKey<N, S>::Type  Key;
        typedef NTreeCell<T, N, S>             Cell;
        
    private:
        Cell root_;
        unsigned long max_depth_;

    public:
        /// \brief Constructor with arbitrary bounds in each dimension.
        ///
        /// \param min       Minimum bound.
        /// \param max       Maximum bound.
        /// \param max_depth Maximum tree depth, or when to stop sub-division.
        NTree(const Key& min, const Key& max, unsigned long max_depth = 8);

        /// \brief Insert a value into the tree.
        ///
        /// Throws an exception if the key is out of bound.
        void insert(const Key& k, const Value& v) throw (NTreeOutOfBounds);

        /// \brief Return the current size of the tree.
        ///
        /// Warning: Iterate through the whole tree, does not cache the result.
        size_t size() const;

        /// \brief Find values with a square search radius.
        /// 
        /// NOTE: Test function: Only return the amount of found values.
        size_t find(const Key& k, const Scalar& radius) const;

        /// \brief Find values within a square search radius and apply the given
        /// function.
        /// 
        /// Operates and key,value pairs.
        ///
        void find(
            const Key& k, 
            const S& radius, 
            boost::function<void (const std::pair<Key, Value>&)> fun) const;

        /// \brief Clears all data from the tree.
        ///
        /// \param keep_tree If true, keep the tree structure and just clear the
        /// data containers.
        void clear(bool keep_tree = false);

        /// \brief Builds an heat map with the given function.
        /// 
        /// Algorithm:
        ///     for each tree leaf i:
        ///         val_i = 0
        ///         for each value j in leaf i:
        ///             val_i += F::fun(value_j)
        ///
        /// The result is a dense matrix indexed in row-major order.
        /// The given vector will be resized to maxLeavesCount().
        ///
        /// The F template parameter class needs to implement a static method
        /// Scalar F::fun(const Value& v).
        ///
        template <class F>
        void heatMap(std::vector<Scalar>& map) const;

        /// \brief Utility function that return the maximum number of leaves the 
        /// tree will have.
        unsigned long maxLeavesCount() const;

        /// \brief Return the discrete cell grid size in each dimension.
        ///
        /// Corresponds to 2^max_depth.
        ///
        unsigned long gridSize() const;

    };

}

// Separate implementation details from the main header:
#include "ntree_impl.hpp"

#endif

