///
/// \file   container_impl.hpp 
/// \brief  Container class implementation.
/// \author François Ferland (particles@francoisferland.com)
///
#ifndef CONTAINER_IMPL_HPP
#define CONTAINER_IMPL_HPP

namespace particles
{
    namespace impl
    {
        /// \brief An adapter for functors meant to operate directly on value
        /// types.
        ///
        /// Since the values of the container are not actually in the NTree, and
        /// that some methods meant to be used on the tree structure provide
        /// function pointers to value types (see heatMap for an example), we
        /// need to adapt functors to access the actual values in the storage
        /// vector based on the index given by the tree structure.
        template <class M>
        struct ContainerFunAdapter
        {
        public:
            static const unsigned long N = M::N;

            typedef typename M::T T;
            typedef typename M::S S;
            
            typedef boost::function<S (const T&)> F;

        private:
            const Container<M>* c_;
            F fun_;

        public:
            ContainerFunAdapter(const Container<M>* c, F f):
                c_(c),
                fun_(f)
            {
            }

            S operator()(const unsigned long i)
            {
                return fun_(c_->at(i));
            }

        };
    }

    template <class M>
    Container<M>::Container(
        const Key& min,
        const Key& max,
        const unsigned long max_depth): 
            Tree(min, max, max_depth)
    {
    }

    template <class M>
    void Container<M>::insert(const Value& v)
    {
        size_t i = data_.size();
        data_.push_back(v);
        this->Tree::insert(M::key(v), i);
    }

    template <class M>
    void Container<M>::reindex()
    {
        Tree::clear(true);

        for (size_t i = 0; i < data_.size(); ++i) {
            this->Tree::insert(M::key(data_[i]), i);
        }
    }

    template <class M>
    typename Container<M>::iterator Container<M>::begin()
    {
        return data_.begin();
    }

    template <class M>
    typename Container<M>::const_iterator Container<M>::begin() const
    {
        return data_.begin();
    }

    template <class M>
    typename Container<M>::iterator Container<M>::end()
    {
        return data_.end();
    }

    template <class M>
    typename Container<M>::const_iterator Container<M>::end() const
    {
        return data_.end();
    }

    template <class M>
    const typename Container<M>::Value& Container<M>::at(const size_t i) const
    {
        return data_[i];
    }

    template <class M>
    const typename Container<M>::Value& Container<M>::operator[](
        const size_t i) const
    {
        return at(i);
    }

    template <class M>
    void Container<M>::reserve(const size_t s)
    {
        data_.reserve(s);
    }

    template <class M>
    size_t Container<M>::size() const
    {
        return data_.size();
    }

    template <class M>
    void Container<M>::heatMap(
        std::vector<Scalar>& map,
        boost::function<Scalar (const Value& v)> fun) const
    {
        this->Tree::template heatMap<Value>(map, fun);
    }

    template <class M>
    template <class F>
    void Container<M>::heatMap(std::vector<Scalar>& map) const
    {
        this->Tree::template heatMap<F>(map);
    }

    template <class M>
    unsigned long Container<M>::gridSize() const
    {
        return this->Tree::gridSize();
    }

}


#endif

