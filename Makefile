all: build build/Makefile bin lib
	cd build; make; cd ../;

build: 
	mkdir build; 

build/Makefile:
	cd build; cmake ../;

bin:
	mkdir bin;

lib:
	mkdir lib;

doc: build/Makefile doxyfile.in
	cd build; make doc; cd ../;

clean:
	rm -rf build bin lib doc;

