#include <particles/ntree.hpp>
#include <iostream>
#include <sstream>

#include "test_common.hpp"

namespace {
    static const unsigned int N = 3;

    typedef double                        S;
    typedef int                           Value;
    typedef particles::NTree<Value, N, S> OctoTree;
    typedef OctoTree::Key                 Key;

    
    void testfun(const std::pair<Key, Value>& v)
    {
        std::cerr << "Got v: " << v.second << std::endl;
    }
    
    template <class S, unsigned long N>
    std::string arrayString(const boost::array<S, N>& data)
    {
        std::stringstream s;
        s << "{";
        for (unsigned long i = 0; i < (N-1); ++i) {
            s << data[i] << ", ";
        }
        s << data[N-1] << "}";

        return s.str();

    }

}

int main(int argc, char **argv)
{
    using namespace particles;
    using namespace test;

    // Common data:
    OctoTree::Key min = {-1.0, -1.0, -1.0};
    OctoTree::Key max = { 1.0,  1.0,  1.0};
    OctoTree tree(min, max);
    Value v = 1;

    // Key flattening test.
    OctoTree::Key mid = { 0.0,  0.0,  0.0};
    OctoTree::Key fks[8] = {
        {-1.0, -1.0, -1.0},
        {-1.0, -1.0,  1.0},
        {-1.0,  1.0, -1.0},
        {-1.0,  1.0,  1.0},
        { 1.0, -1.0, -1.0},
        { 1.0, -1.0,  1.0},
        { 1.0,  1.0, -1.0},
        { 1.0,  1.0,  1.0}
    };
    for (unsigned long i = 0; i < 8; ++i) {
        unsigned long fk = impl::flattenKey<N, S>(fks[i], mid);
        if (fk != i) {
            std::cerr << 
                "Failed flat key test " << i << 
                ", got " << fk  << "." << std::endl;
        }
    }

    // Key locations test(s).
    typedef NTreeCellLocation<N>::Type CellLocation;
    CellLocation cls[] = {
        {0, 0, 0},
        {1, 0, 0},
        {0, 1, 0},
        {0, 0, 1},
        {1, 1, 1},
        {1, 0, 1},
        {0, 1, 1}
    };
    unsigned long clks[] = {0, 4, 2, 1, 7, 5, 3};
    unsigned long clis[] = {0, 1, 2, 4, 7, 5, 6};
    CellLocation corner  = {0, 0, 0};
    unsigned long offset = 1; 
    for (int i = 0; i < sizeof(cls) / sizeof(CellLocation); ++i) {
        CellLocation cl = impl::keyLocation<N>(corner, offset, clks[i]);
        if (cl != cls[i]) {
            std::cerr << 
                "Failed cell location test " << i  << ". " <<
                "Expected " << arrayString(cls[i]) << ", " <<
                "got "      << arrayString(cl)     << "."  << std::endl;
        }

        unsigned long cli = impl::locationIndex<N>(cls[i], 2);
        if (cli != clis[i]) {
            std::cerr <<
                "Failed row-major location index test " << i << ". " <<
                "Expected " << clis[i] << ", " <<
                "got "      << cli     << "."  << std::endl; 
        }
    }

    // In-bounds test.
    OctoTree::Key ib_k  = {0.0, 0.0, 0.0};
    bool          ib_ok = true;
    try {
        tree.insert(ib_k, v);
    } catch (NTreeException e) {
        std::cerr << e.what() << std::endl;
        ib_ok = false;
    }
    if (!ib_ok)
        std::cerr << "Failed in-bounds test." << std::endl;

    // Out-of-bounds test.
    OctoTree::Key oob_k  = {2.0, 0.0, 0.0};
    bool          oob_ok = false;
    try {
        tree.insert(oob_k, v);
    } catch (NTreeOutOfBounds) {
        // Ignore exception, where expecting it.
        oob_ok = true;
    } catch (NTreeException e) {
        std::cerr << e.what() << std::endl;
    }
    if (!oob_ok)
        std::cerr << "Failed oob test." << std::endl;

    // Final size test.
    size_t fs = tree.size();
    if (fs != 1) {
        std::cerr << 
            "Failed final size test, got " << fs << 
            ", should be 1." << std::endl;
    }

    // Search test (count for now).
    OctoTree::Key center = {-1.0, -1.0, -1.0};
    OctoTree::Key k2     = {-0.8, -0.8, -0.8};
    tree.insert(k2, v + 1);
    size_t sc = 0;
    if ((sc = tree.find(center, 0.5)) != 1) {
        std::cerr << 
            "Failed search test with a small radius, " <<
            "got " << sc << ", " << 
            "should be 1." << std::endl;
    }
    if ((sc = tree.find(center, 2.0)) != 2) {
        std::cerr <<
            "Failed search test with large radius, " <<
            "got " << sc << ", " <<
            "should be 2." << std::endl;
    }

    // Search vector test.
    typedef std::vector< std::pair<OctoTree::Key, Value> > SearchResult;
    SearchResult sr;
    tree.find(center, 0.5, boost::bind(&SearchResult::push_back, &sr, _1));
    //tree.find(center, 0.5, testfun);
    if (sr.size() != 1) {
        std::cerr << "Failed search test with predicate, got empty return." << 
            std::endl;
    } else {
        const OctoTree::Key& kr = sr[0].first;
        const Value&         vr = sr[0].second;

        if (vr != (v+1)) {
            std::cerr << "Failed value retrieval, " <<
                "got " << vr << 
                ", expected " << (v+1) << 
                std::endl;
        }

        if (kr != k2) {
            std::cerr << "Failed key retrieval." << std::endl;
        }
    }

    // Large insert set test.
    OctoTree large(min, max);
    int large_count = 1e6;
    if (argc == 2)
        large_count = atoi(argv[1]);
    std::cerr << "Insert test for set of " << large_count << "..." << std::endl;
    clock_t start = clock();
    for (int i = 0; i < large_count; ++i) {
        OctoTree::Key k = {
            2.0 * (double(rand()) / RAND_MAX) - 1.0,
            2.0 * (double(rand()) / RAND_MAX) - 1.0,
            2.0 * (double(rand()) / RAND_MAX) - 1.0
        };
        large.insert(k, rand());
    }
    std::cerr << "Insert time: " << 
        1000.0 * (double(clock() - start) / CLOCKS_PER_SEC) << " ms" <<
        std::endl;

    OctoTree::Key lc = {0.0, 0.0, 0.0};
    start = clock();
    int c01 = large.find(lc, 0.1);
    std::cerr << "Count with radius 0.1: " << c01 << 
        std::endl;
    printTime(start, "Search time");
    start = clock();
    int c05 = large.find(lc, 0.5);
    std::cerr << "Count with radius 0.5: " << c05 << 
        std::endl;
    printTime(start, "Search time");
    start = clock();
    int c10 = large.find(lc, 1.0);
    std::cerr << "Count with radius 1.0: " << c10 << 
        std::endl;
    printTime(start, "Search time");

    // Heat map generation test.
    std::vector<S> heatmap;
    std::cerr << "Generating heat map ..." << std::endl;
    start = clock();
    tree.heatMap< test::identity<S> >(heatmap);
    printTime(start, "Generation time");

    return 0;

}

